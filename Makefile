CC := gcc
CFLAGS := -O2 -Wall -Wpedantic -Wextra
CLIBS := 
OUT := http-server


all:
	gcc -o $(OUT) http.c sock.c $(CFLAGS) $(CLIBS)
clean:
	rm -f *.o
