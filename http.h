#ifndef HTTP_H
#define HTTP_H

#include <stdint.h>
#include <linux/limits.h>

#ifndef BUF_SIZE
#define BUF_SIZE 4096
#endif /* BUF_SIZE */

struct http_info {
    uint16_t buf_len;       /* Total length of used space in buffer (ends at nul-char) */
    uint16_t req_method;    /* METHOD_TYPE */
    uint16_t req_len;      /* Length of data between method and newline */
    uint16_t status_code;
};

enum METHOD_TYPE {
    GET = 1,
    PUT,
    HEAD,
    POST
};

#define STATUS_200 "HTTP/1.1 200 OK\r\n"
#define STATUS_400 "HTTP/1.1 400 Bad Request\r\n"

extern char net_buf[BUF_SIZE];
extern char pathname[PATH_MAX];

int send_html_file(int cfd, char *pathname);

#endif /* HTTP_H */