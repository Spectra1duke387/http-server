#include <arpa/inet.h>
#include <ctype.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "http.h"

#define PORT_NUM    8080

#ifndef BACKLOG
#define BACKLOG     5
#endif /* BACKLOG */

static void
init_addr(struct sockaddr_in *addr)
{
    memset(addr, 0, sizeof(*addr));

    addr->sin_family = AF_INET;
    addr->sin_port = htons(PORT_NUM);
    addr->sin_addr.s_addr = INADDR_ANY;

    return;
}

static int
inet_init(void)
{
    int sfd;
    static struct sockaddr_in addr;

    init_addr(&addr);

    sfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sfd == -1) {
        perror("socket()");
        return -1;
    }

    if (bind(sfd, (struct sockaddr *) &addr, sizeof(struct sockaddr_in)) == -1) {
        perror("bind()");
        return -1;
    }

    if (listen(sfd, BACKLOG) == -1) {
        perror("listen()");
        return -1;
    }

    return sfd;
}

int sock_io(int cfd, char *buf)
{
    int num_read;

    num_read = read(cfd, buf, BUF_SIZE);
    printf("num_read: %i\n%s\n", num_read, buf);

    if (write(cfd, STATUS_200, sizeof(STATUS_200)) != sizeof(STATUS_200)) {
        fprintf(stderr, "Failed/partial write\n");
        exit(EXIT_FAILURE);
    }

    send_html_file(cfd, pathname);

    return 0;
}

/* SERVER functions */
int handle_request(void)
{
    int sfd, cfd;
    struct sockaddr_in claddr;
    char claddr_str[INET_ADDRSTRLEN];
    socklen_t claddr_len;

    claddr_len = sizeof(struct sockaddr_in);

    sfd = inet_init();
    if (sfd == -1) {
        return -1;
    }

    printf("blocking for accept\n");
    cfd = accept(sfd, (struct sockaddr *) &claddr, &claddr_len);
    if (cfd < 0) {
        perror("accept()");
        return -1;
    }

    if (inet_ntop(AF_INET, &(claddr.sin_addr), claddr_str,
            INET_ADDRSTRLEN) == NULL)
        printf("Couldn't convert client address to string\n");
    else
        printf("Server received packets from %s\n", claddr_str);
    
    if (sock_io(cfd, net_buf) == -1)
        return -1;

    close(cfd);
    return 0;
}
