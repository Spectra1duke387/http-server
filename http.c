#include <errno.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "http.h"
#include "sock.h"

typedef uint8_t bool;
#define true    1
#define false   0


/* Find bitmask for this in http.h */

struct http_info http_req;
char net_buf[BUF_SIZE] = {0, };
char pathname[PATH_MAX] = "example.html";

static bool
is_crlf(char *buffer)
{
    if (*buffer == '\r' && *(buffer + 1) == '\n')
        return true;
    else
        return false;
}

static int
get_req_info(char *buffer)
{
    int offset, i;
    switch (http_req.req_method) {
    case GET:   /* FALLTHROUGH */
    case PUT:
        offset = 4;
        break;
    case HEAD:  /* FALLTHROUGH */
    case POST:
        offset = 5;
        break;
    default:
        /* We shouldn't get here... */
        return -EINVAL;
    }

    buffer += offset;
    for (i = 0; i < http_req.buf_len - offset; i++) {
        if (is_crlf(buffer)) {
            http_req.req_len = i;
            return 0;
        }
        ++buffer;
    }

    return -ENOMSG;
}

/* Return length of method, buffer
 * gets changed to start of method */
int http_method(char *buffer)
{
    int ret_val, i;
    bool is_http_11 = false;

    if (memcmp(buffer, "GET", 3) == 0) {
        http_req.req_method = GET;
    } else if (memcmp(buffer, "PUT", 3) == 0) {
        http_req.req_method = PUT;
    } else if (memcmp(buffer, "HEAD", 4) == 0) {
        http_req.req_method = HEAD;
    } else if (memcmp(buffer, "POST", 4) == 0) {
        http_req.req_method = POST;
    } else {
        return -EINVAL;
    }

    if ((ret_val = get_req_info(buffer)) < 0)
        return ret_val;

    switch (http_req.req_method) {
    case GET:
        for (i = 0; i < http_req.buf_len; i++) {
            if (strncmp(&net_buf[i], "HTTP/1.1", sizeof("HTTP/1.1")) == 0)
                is_http_11 = true;
        }
        strncpy(net_buf, STATUS_400, sizeof(STATUS_400));
        break;
    case PUT:
        break;
    case HEAD:
        break;
    case POST:
        break;
    default:
        return -EINVAL;
    }

    return 0;
}

static uint16_t
get_buf_len(char *buffer)
{
    int i;
    for (i = 0; i < BUF_SIZE; i++) {
        if (*(buffer++) == '\0')
            return i;
    }

    return -1;
}

static int
init(void)
{
    memset(&http_req, 0, sizeof(http_req));

    return 0;
}

static int
http_req_init(char *buffer)
{
    http_req.buf_len = get_buf_len(buffer);
    return http_method(buffer);
}

int send_html_file(int cfd, char *pathname)
{
    int hfd;
    struct stat st;

    hfd = open(pathname, O_RDONLY);
    if (hfd == -1) {
        perror("open()");
        return -1;
    }

    if (stat(pathname, &st) == -1) {
        perror("stat()");
        return -1;
    }

    sendfile(cfd, hfd, NULL, st.st_size);

    return 0;
}

int main(void)
{
    int ret_val;
    
    init();

    if ((ret_val = handle_request()) < 0) {
        fprintf(stderr, "error code: %i", ret_val);
        exit(EXIT_FAILURE);
    }

    http_req_init(net_buf);
    
    printf("buffer len: %d\nmethod type: %d\nreq_len: %d\n", http_req.buf_len, http_req.req_method, http_req.req_len);

    return 0;
}
